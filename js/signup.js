const form = document.getElementById('form');
const firstName = document.getElementById('first_name');
const lastName = document.getElementById('last_name');
const email = document.getElementById('email');
const password = document.getElementById('password');
const passwordConfirm = document.getElementById('password-confirm');
const tos = document.getElementById('tos');
const invalid = document.getElementById('invalid');
const formHeader = document.getElementById('form-header');
const iUnderstandButton = document.getElementById('i-understand');
const termsAndConditions = document.getElementsByClassName('terms-and-conditions')[0];
const tosLink = document.getElementsByClassName('tos-link')[0];
const termsCloseButton = document.getElementById('terms-close');

/* error text messages*/
const invalidFirstName = document.getElementById('invalid-first_name');
const invalidLastName = document.getElementById('invalid-last_name');
const invalidEmail = document.getElementById('invalid-email');
const invalidPassword = document.getElementById('invalid-password');
const invalidPasswordConfirm = document.getElementById('invalid-password-confirm');
const invalidTos = document.getElementById('invalid-tos');

window.onload = () => {
    try {
        loginDetails = JSON.parse(localStorage.getItem('signup'));
        document.querySelector('.sign-up-container').innerHTML = `<h2>Welcome, ${loginDetails.firstName} ${loginDetails.lastName}</h2>
        <p>You have already Signed Up.</p>`;
    } catch (error) {
        console.log(error);
    }

}

function hasNumbers(name) {

    const specialCharacters = ['~', '!', '@', '#', '$', '%', '*', '(', ')', '+', '=', ':'];

    const flag = name.split("").reduce((flagStatus, character) => {

        if ((character >= '0' && character <= '9') || specialCharacters.indexOf(character) > -1) {
            flagStatus = true;
        }

        return flagStatus;
    }, false);

    return flag;
}

form.addEventListener('submit', (event) => {
    let hasError = false;

    event.preventDefault();

    if (firstName.value.length === 0 || firstName.value.indexOf(" ") > -1 || hasNumbers(firstName.value)) {
        invalidFirstName.innerText = "Please make sure your first name contains only alphabets with no space";
        firstName.style["border"] = "1px solid #ccc";
        firstName.style["border-bottom"] = "3px solid #e87c03";
        hasError = true;
    } else {
        firstName.style["border-bottom"] = "1px solid #00FF00";
        invalidFirstName.innerText = "";
    }

    if (lastName.value.length === 0 || lastName.value.indexOf(" ") > -1 || hasNumbers(lastName.value)) {
        invalidLastName.innerText = "Please make sure your last name contains only alphabets with no space";
        lastName.style["border"] = "1px solid #ccc";
        lastName.style["border-bottom"] = "3px solid #e87c03";
        hasError = true;
    } else {
        lastName.style["border-bottom"] = "1px solid #00FF00";
        invalidLastName.innerText = "";
    }

    const pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (pattern.test(email.value) === false) {
        invalidEmail.innerText = "Please enter correct email pattern e.g. yourname@mail.com";
        email.style["border"] = "1px solid #ccc";
        email.style["border-bottom"] = "3px solid #e87c03";
        hasError = true;
    } else {
        email.style["border-bottom"] = "1px solid #00FF00";
        invalidEmail.innerText = "";
    }

    if (password.value.length < 8) {
        invalidPassword.innerText = "Please make sure your password has atleast 8 characters";
        password.style["border"] = "1px solid #ccc";
        password.style["border-bottom"] = "3px solid #e87c03";
        hasError = true;
    } else {
        invalidPassword.innerText = "";
        password.style["border-bottom"] = "1px solid #00FF00";
    }

    if (passwordConfirm.value.length < 8 || passwordConfirm.value !== password.value) {
        invalidPasswordConfirm.innerText = "Please make sure both passwords match and have atleast 8 characters";
        passwordConfirm.style["border"] = "1px solid #ccc";
        passwordConfirm.style["border-bottom"] = "3px solid #e87c03";
        hasError = true;
    } else {
        passwordConfirm.style["border-bottom"] = "1px solid #00FF00";
        invalidPasswordConfirm.innerText = "";
    }


    if (tos.checked === false) {
        invalidTos.innerText = "Please accept our terms and conditions";
        hasError = true;
    } else {
        invalidTos.innerText = "";
    }

    if (!hasError) {
        const loginDetails = {
            'firstName': firstName.value,
            'lastName': lastName.value,
        };

        localStorage.setItem('signup', JSON.stringify(loginDetails));

        form.style.display = "none";
        formHeader.innerHTML = `<i style="color: green" class="fa fa-check" aria-hidden="true"></i>Signup Successful
        <br/><div style='font-size: 15px; text-align: center;'> Redirecting you to main page in 3 sec.</div>`;
        formHeader.style["margin-top"] = "2em";

        setTimeout(() => {
            window.location = "./index.html";
        }, 3000);
    }
});

termsCloseButton.addEventListener('click', (event) => {
    termsAndConditions.style.display = 'none';
});

tosLink.addEventListener('click', (event) => {
    termsAndConditions.style.display = 'block';
});

iUnderstandButton.addEventListener('click', (event) => {
    termsAndConditions.style.display = 'none';
    tos.checked = true;
});
