const categoryListed = document.getElementById('catergory-listed');
const productCards = document.getElementById('product-cards');
const productsDiv = document.querySelector('.products');
const url = 'https://fakestoreapi.com/products';

const fetchfromURL = (endpoint) => {
    return new Promise((resolve, reject) => {
        fetch(endpoint)
            .then((response) => {
                if (!response.ok) {
                    const errorResponce = `Could not fetch Products`;
                    reject(errorResponce);
                }
                return response.json();
            })
            .then((responseData) => {
                resolve(responseData);
            })
            .catch((error) => {
                const errorResponce = `Check your Internet Connection`;
                reject(errorResponce);
            });
    });
}

function handleCategory() {
    const categoryCheckBoxes = document.querySelectorAll('.category-checkbox');
    let checkbox = document.getElementById('is-category');
    const isAllcategory = document.getElementById('all').checked;
    if (isAllcategory) {
        checkbox.checked ? renderWithCategory() : renderWithoutCategory();
    } else {
        const whichCategories = [];
        categoryCheckBoxes.forEach((checkbox) => {
            if (checkbox.checked) {
                whichCategories.push(checkbox.value);
            }
        });
        if (checkbox.checked === true) {
            renderWithCategory(whichCategories);
        } else {
            renderWithoutCategory(whichCategories);
        }
    }

}

const rendercategories = (products) => {
    const categories = products.map((product) => {
        return product.category;
    })
        .filter((category, index, categoryListed) => {
            return categoryListed.indexOf(category) === index;
        });
    allCategories = categories;
    categories.forEach((category) => {
        categoryListed.innerHTML += `<label class="label">
    <input type="checkbox" class='category-checkbox' onChange='categoryManage()' id='${category.replaceAll('\'', '').replaceAll(' ', '')}'name="${category}" value="${category}" /> ${category}
  </label>`;
    });
}


const renderProducts = (jsonData) => {

    jsonData.forEach((product) => {
        const starts = Math.floor(product.rating.rate);
        const partialStar = product.rating.rate - starts;
        const leftStars = 5 - starts;
        let startsHtml = '';
        for (let index = 0; index < starts; index++) {
            startsHtml += `<i class="fa fa-star"></i>`;
        }
        for (let index = 0; index < leftStars; index++) {
            startsHtml += `<i class="fa fa-star-o"></i>`;
        }

        productCards.innerHTML += `<div class="product-card" id="${product.id}" onClick='viewThisProduct(this)'>
  <div class="product-image">
    <img src="${product.image}" alt="" />
  </div>
  <div class="product-details">
    <h3>${product.title}</h3>
    ${startsHtml}
    <span>${product.rating.rate}</span>
    (<span class='count'>${product.rating.count}</span>)
    <h4><small>$</small><span class='price'>${product.price.toFixed(2)}<span></h4>
    <p>
    ${product.description}
    </p>
  </div>
</div>`;
    });
}

let products;
let allCategories;

(function () {
    fetchfromURL(url)
        .then((data) => {
            products = data;
            rendercategories(data);
            renderWithCategory();
        })
        .catch((error) => {
            document.querySelector('.products-loading').innerHTML = `${error}`;

        });
})();

function renderWithCategory(categoryList = allCategories) {
    productsDiv.innerHTML = '';
    if (categoryList && categoryList?.length === 0) {
        productsDiv.innerHTML += `<h3>Choose some Categories to display products</h3>`;
    } else {
        allCategories.forEach((category) => {
            if (categoryList.includes(category)) {
                const categoryProducts = products.filter((product) => {
                    return product.category === category;
                });
    
                let productContent = '';
    
                categoryProducts.forEach((product) => {
                    const starts = Math.floor(product.rating.rate);
                    const partialStar = product.rating.rate - starts;
                    const leftStars = 5 - starts;
                    let startsHtml = '';
                    for (let index = 0; index < starts; index++) {
                        startsHtml += `<i class="fa fa-star"></i>`;
                    }
                    for (let index = 0; index < leftStars; index++) {
                        startsHtml += `<i class="fa fa-star-o"></i>`;
                    }
    
                    productContent += `<div class="product-card" id="${product.id}" onClick='viewThisProduct(this)'>
              <div class="product-image">
                <img src="${product.image}" alt="" />
              </div>
              <div class="product-details">
                <h3>${product.title}</h3>
                ${startsHtml}
                <span>${product.rating.rate}</span>
                (<span class='count'>${product.rating.count}</span>)
                <h4><small>$</small><span class='price'>${product.price.toFixed(2)}<span></h4>
                <p>
                ${product.description}
                </p>
              </div>
            </div>`;
                });
    
                productsDiv.innerHTML += `<h2>${category}</h2>
            <div id="product-cards">${productContent}</div>`;
    
            }
    
        });
        
    }
    

}

function renderWithoutCategory(categoryList = allCategories) {
    productsDiv.innerHTML = '';
    let productContent = '';
    products.forEach((product) => {
        if (categoryList.includes(product.category)) {
            const starts = Math.floor(product.rating.rate);
            const partialStar = product.rating.rate - starts;
            const leftStars = 5 - starts;
            let startsHtml = '';
            for (let index = 0; index < starts; index++) {
                startsHtml += `<i class="fa fa-star"></i>`;
            }
            for (let index = 0; index < leftStars; index++) {
                startsHtml += `<i class="fa fa-star-o"></i>`;
            }

            productContent += `<div class="product-card" id="${product.id}" onClick='viewThisProduct(this)'>
      <div class="product-image">
        <img src="${product.image}" alt="" />
      </div>
      <div class="product-details">
        <h3>${product.title}</h3>
        ${startsHtml}
        <span>${product.rating.rate}</span>
        (<span class='count'>${product.rating.count}</span>)
        <h4><small>$</small><span class='price'>${product.price.toFixed(2)}<span></h4>
        <p>
        ${product.description}
        </p>
      </div>
    </div>`;

        }

    });
    if (categoryList && categoryList?.length === 0) {
        productsDiv.innerHTML += `<h3>Choose some Categories to display products</h3>`;
    } else {
        productsDiv.innerHTML += `<h2>Top Products</h2>
    <div id="product-cards">${productContent}</div>`;
    }


}

function categoryManage() {
    const categoryCheckBoxes = document.querySelectorAll('.category-checkbox');
    const categoryAll = document.getElementById('all').checked = false;
    const isByCategory = document.getElementById('is-category').checked;
    const whichCategories = [];
    categoryCheckBoxes.forEach((checkbox) => {
        if (checkbox.checked) {
            whichCategories.push(checkbox.value);
        }
    });
    if (isByCategory) {
        renderWithCategory(whichCategories);
    } else {
        renderWithoutCategory(whichCategories);
    }
}

function resetCategory() {
    const categoryCheckBoxes = document.querySelectorAll('.category-checkbox');
    const isByCategory = document.getElementById('is-category').checked;
    categoryCheckBoxes.forEach((checkbox) => {
        if (checkbox.value !== 'all') {
            checkbox.checked = false;
        }
    });
    const whichCategories = [];
    categoryCheckBoxes.forEach((checkbox) => {
        if (checkbox.checked) {
            whichCategories.push(checkbox.value);
        }
    });
    const isAllcategory = document.getElementById('all').checked;
    if (isAllcategory) {
        isByCategory ? renderWithCategory() : renderWithoutCategory();
    } else {
        isByCategory ? renderWithCategory(whichCategories) : renderWithoutCategory(whichCategories);
    }

}

function viewThisProduct(element) {
    document.querySelector('.categories').innerHTML = `<h1 id='back-button' onClick='loadMainPage()'><i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
    </h1>`;
    productsDiv.innerHTML = `<div class="products-loading">
    <div class="loader"></div>
  </div>`;
    fetchfromURL(`${url}/${element.id}`)
        .then((data) => {
            renderSingleProduct(data);
        })
        .catch((error) => {
            document.querySelector('.products-loading').innerHTML = `${error}`;
        });
}

function renderSingleProduct(data) {
    const starts = Math.floor(data.rating.rate);
    const partialStar = data.rating.rate - starts;
    const leftStars = 5 - starts;
    let startsHtml = '';
    for (let index = 0; index < starts; index++) {
        startsHtml += `<i class="fa fa-star"></i>`;
    }
    for (let index = 0; index < leftStars; index++) {
        startsHtml += `<i class="fa fa-star-o"></i>`;
    }
    productsDiv.innerHTML = `<h2>${data.title}</h2>
    <div class="product-card-single" id="${data.id}" '>
      <div class="product-image">
        <img src="${data.image}" alt="" />
      </div>
      <div class="product-details">
        <h3>Category: ${data.category}</h3>
        ${startsHtml}
        <span>${data.rating.rate}</span>
        (<span class='count'>${data.rating.count}</span>)
        <h4><small>$</small><span class='price'>${data.price.toFixed(2)}<span></h4>
        
        <p>
        <h3>Description:</h3>
        ${data.description}
        </p>
      </div>
    </div>`;
}


function loadMainPage() {
    document.location.href = './index.html';
}
